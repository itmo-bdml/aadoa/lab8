import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import networkx as nx

from kruskal.Graph import Graph

matplotlib.use('TkAgg')
np.random.seed(42)

VERTICES: int = 30
EDGES: int = 70
FIG_SIZE: tuple[int, int] = (16, 12)
MIN_WEIGHT: int = 1
MAX_WEIGHT: int = 50


def generate_graph(vertices: int, edges: int, min_weight: int = 1, max_weight: int = 1) -> tuple[list[tuple], np.ndarray]:
    """
    generates adjacency list and adjacency matrix for an unordered weighted graph with edges and vertices
    with integer weights from [min_weight, max_weight]
    :param vertices:
    :param edges:
    :param min_weight:
    :param max_weight:
    :return: adjacency list and a square adjacency matrix
    """
    adjacency_matrix = np.zeros((vertices, vertices))
    adjacency_list = []
    i = 0
    while i < edges:
        v1, v2 = np.random.randint(low=0, high=vertices, size=2)
        if v1 == v2 or adjacency_matrix[v1, v2]:
            continue
        weight = np.random.randint(low=min_weight, high=max_weight, size=1)[0]
        adjacency_matrix[v1, v2] = weight
        adjacency_matrix[v2, v1] = weight
        adjacency_list.append((v1, v2, weight))
        i += 1
    return adjacency_list, adjacency_matrix


adjacency_list, adjacency_matrix = generate_graph(VERTICES, EDGES, MIN_WEIGHT, MAX_WEIGHT)
G = nx.from_numpy_array(adjacency_matrix)
pos = nx.drawing.spring_layout(G)

plt.figure(figsize=FIG_SIZE)
nx.draw_networkx(G, font_size=8, pos=pos)
plt.title("Original Graph")
plt.savefig("./plots/original.png")

plt.show()

g = Graph(VERTICES)
for u, v, w in adjacency_list:
    g.add_edge(u, v, w)
res = g.KruskalMST()

new_graph = np.zeros((VERTICES, VERTICES))
for u, v, w in res:
    new_graph[u, v] = w
    new_graph[v, u] = w

new_G = nx.from_numpy_array(new_graph)
new_pos = nx.drawing.spring_layout(new_G)

plt.figure(figsize=FIG_SIZE)
nx.draw_networkx(new_G, font_size=8, pos=pos)
plt.title("Minimum Spanning Tree")
plt.savefig("./plots/mst.png")
plt.show()