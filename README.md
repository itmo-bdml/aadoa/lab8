## Lab 8

### Custom algorithm. 

---
[Report (google doc)](https://docs.google.com/document/d/1hI-pKRYR5K3KI8zCTRQV0EtDr94hPO8xMvEkSufOphg) 

Performed by:
* Elya Avdeeva
* Gleb Mikloshevich


[Original paper: Grid-based angle-constrained path planning](https://arxiv.org/pdf/1506.01864.pdf) 

---
### Results

#### Kruskal's algorithm


Original graph

<img src="./kruskal/plots/original.png" height="350">

Minimum Spanning Tree generated with Kruskal's algorithm

<img src="./kruskal/plots/mst.png" height="350">




#### LiAn algorithm
start: (165, 305)

finish: (1289, 689)

**Generated path with step 5 and angle 30:**

<img src="./lab/plots/path_on_map_5_30.png" height="350">

It took approximately 2.000.000 points to calculate the path.

**Generated path with step 15 and angle 15:**

<img src="./lab/plots/path_on_map_15_15.png" height="350">

The algorithm visited 2.559.698 points to calculate the path.

**Generated path with step 30 and angle 30:**

<img src="./lab/plots/path_on_map_30_30.png" height="350">

The algorithm visited 5.541.634 points to calculate the path.
