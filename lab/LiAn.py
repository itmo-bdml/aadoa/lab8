from Point import Point
import utils
import numpy as np
from queue import PriorityQueue
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
from pyinstrument import Profiler


def LiAn_algorithm(m: np.ndarray, start: Point, finish: Point, step_distance: float, angle: float):
    """

    :param m: 2d matrix map
    :param start: start point
    :param finish: finish pont
    :param step_distance: float
    :param angle:
    :return:
    """
    q = PriorityQueue()
    closed = set()
    path = []
    distances = np.full(m.shape, np.inf)
    visited = np.zeros(m.shape)

    path_was_found = False

    q.put((0, start))
    iterations = 0

    def expand(cur_point: Point):
        """

        :param cur_point:
        :return:
        """
        successors = utils.get_circle_successors(cur_point, step_distance)
        if cur_point.distance_to(finish) <= step_distance:
            successors.append(finish)

        for point in successors:
            if point.y < 0 or point.x < 0 or point.y >= m.shape[0] or point.x >= m.shape[1]:
                continue

            a = 0
            if cur_point != start:
                a = utils.calculate_angle(cur_point.previous_point, cur_point, point)
                if a > angle:
                    continue
            # if cur_point.previous_point is not None and (cur_point, cur_point.previous_point) in closed:
            if cur_point.previous_point is not None and (cur_point, a) in closed:
                continue
            if utils.border_on_a_line_optim(m, cur_point, point):
                continue
            point.previous_point = cur_point
            point.distance = cur_point.distance + cur_point.distance_to(point)
            point.angle = a
            q.put((point.distance_to(finish) + point.distance, point))

    while not q.empty():
        iterations += 1
        weight, cur_point = q.get()

        if cur_point == finish:
            path_was_found = True
            print("Yes")
            break
        if cur_point.previous_point is not None and (cur_point, cur_point.previous_point) in closed:
            continue
        distances[cur_point.y, cur_point.x] = cur_point.distance if np.isinf(distances[cur_point.y, cur_point.x]) else distances[cur_point.y, cur_point.x]
        visited[cur_point.y, cur_point.x] = 1
        if iterations % 5e5 == 0:
            print(iterations, cur_point)
        expand(cur_point)
        # closed.add((cur_point, cur_point.previous_point))
        closed.add((cur_point, cur_point.angle))

    while cur_point is not None:
        path.append(cur_point)
        cur_point = cur_point.previous_point
    print(f"iterations: {iterations}")
    return list(reversed(path)), visited, distances


if __name__ == '__main__':
    # params
    with open('./map.npy', 'rb') as f:
        m = np.load(f)
    start_point = Point(165, 305)
    finish_point = Point(1289, 689)
    step_distance = 5
    angle = 30

    # profiler and algorithm
    profiler = Profiler()
    profiler.start()
    p, visited, distances = LiAn_algorithm(m, start_point, finish_point, step_distance, angle)
    profiler.stop()
    html = profiler.output_html()
    with open(f'./profiler/result_{step_distance}_{angle}_v4.html', 'w') as f:
        f.write(html)
    profiler.print()

    # visualisation
    map_with_path = m.copy()
    colored_map = Image.open(r'./karta.bmp')
    img_draw = ImageDraw.Draw(colored_map)

    for i in range(len(p)-1):
        line_points = utils.get_line_points(p[i], p[i+1])
        for point in line_points:
            img_draw.line(((p[i].x, p[i].y), (p[i+1].x, p[i+1].y)), fill=(255, 0, 0), width=2)
    plt.imshow(img_draw._image)
    plt.savefig(f"./plots/path_on_map_{step_distance}_{angle}_v4.png")
    plt.show()
