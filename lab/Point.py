class Point:
    x: None
    y: None
    distance: float = 0
    weight: float = 0
    angle: float = 0

    def __init__(self, x, y, weight=None, previous_point=None, distance=0):
        self.x = x
        self.y = y
        self.weight = weight
        self.previous_point = previous_point
        self.distance = 0
        self.angle = 0

    def distance_to(self, other):
        return Point.calc_euclidean_distance(self, other)

    def __eq__(self, other):
        if self.x == other.x and self.y == other.y:
            return True
        return False

    def __repr__(self):
        return f"Point({self.x}, {self.y})"

    def __str__(self):
        return f"Point({self.x}, {self.y})"

    def __lt__(self, other):
        if self.x < other.x:
            return True
        if self.y < other.y:
            return True
        return False

    def __le__(self, other):
        if self.x <= other.x:
            return True
        if self.y <= other.y:
            return True
        return False


    def __hash__(self):
        return 10000*self.y + self.x

    @staticmethod
    def calc_euclidean_distance(point1, point2):
        return ((point1.x - point2.x)**2 + (point1.y - point2.y)**2)**0.5
