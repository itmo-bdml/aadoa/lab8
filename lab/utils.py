import numpy as np
from Point import Point
import math


def get_circle_successors(center: Point, r: float) -> list[Point]:
    """
    The function calculates points on a circle from the center with radius r
    :param center: Point in the circle center
    :param r: float. circle radius
    :return: list[Point] with a circle points
    """
    points: list[Point] = []
    x = r
    y = 0
    points.append(Point(center.x + r, center.y, previous_point=center))
    points.append(Point(center.x, center.y + r, previous_point=center))
    points.append(Point(center.x, center.y - r, previous_point=center))
    points.append(Point(center.x - r, center.y, previous_point=center))
    P = 1 - r

    while x > y:
        y += 1
        if P <= 0:
            P = P + 2 * y + 1
        else:
            x -= 1
            P = P + 2 * y - 2 * x + 1
        if x < y:
            return points

        points.append(Point(x + center.x, y + center.y, previous_point=center))
        points.append(Point(-x + center.x, y + center.y, previous_point=center))
        points.append(Point(x + center.x, -y + center.y, previous_point=center))
        points.append(Point(-x + center.x, -y + center.y, previous_point=center))

        if x != y:
            points.append(Point(y + center.x, x + center.y, previous_point=center))
            points.append(Point(-y + center.x, x + center.y, previous_point=center))
            points.append(Point(y + center.x, -x + center.y, previous_point=center))
            points.append(Point(-y + center.x, -x + center.y, previous_point=center))

    for p in points:
        p.distance = center.distance + p.distance_to(center)
        p.previous_point = p

    return points


def get_line_points(start: Point, finish: Point) -> tuple[list[int], list[int]]:
    x1 = start.x
    x2 = finish.x
    y1 = start.y
    y2 = finish.y
    points_y = []
    points_x = []
    issteep = abs(y2 - y1) > abs(x2 - x1)
    if issteep:
        x1, y1 = y1, x1
        x2, y2 = y2, x2
    rev = False
    if x1 > x2:
        x1, x2 = x2, x1
        y1, y2 = y2, y1
        rev = True
    deltax = x2 - x1
    deltay = abs(y2 - y1)
    error = int(deltax / 2)
    y = y1
    ystep = None
    if y1 < y2:
        ystep = 1
    else:
        ystep = -1
    for x in range(x1, x2 + 1):
        if issteep:
            points_x.append(y)
            points_y.append(x)
        else:
            points_x.append(x)
            points_y.append(y)
        error -= deltay
        if error < 0:
            y += ystep
            error += deltax
    # Reverse the list if the coordinates were reversed
    if not rev:
        points_x = reversed(points_x)
        points_y = reversed(points_y)
    # return points
    return points_x, points_y


def border_on_a_line_optim(m: np.ndarray, start: Point, finish: Point) -> bool:
    """
    The function calculates if a straight line from start to finish meets an obstacle
    :param m: 2d map where 0 is an empty cell and one is an obstacle
    :param start: start Point
    :param finish: finish Point
    :return: True if path collides with obstacle and False otherwise
    """

    x1 = start.x
    x2 = finish.x
    y1 = start.y
    y2 = finish.y
    issteep = abs(y2 - y1) > abs(x2 - x1)
    if issteep:
        x1, y1 = y1, x1
        x2, y2 = y2, x2
    if x1 > x2:
        x1, x2 = x2, x1
        y1, y2 = y2, y1
    deltax = x2 - x1
    deltay = abs(y2 - y1)
    error = int(deltax / 2)
    y = y1
    ystep = None
    if y1 < y2:
        ystep = 1
    else:
        ystep = -1
    for x in range(x1, x2 + 1):
        if issteep:
            if m[x, y] == 1:
                return True
        else:
            if m[y, x] == 1:
                return True
        error -= deltay
        if error < 0:
            y += ystep
            error += deltax

    return False


def border_on_a_line(m: np.ndarray, start: Point, finish: Point) -> bool:
    """
    The function calculates if a straight line from start to finish meets an obstacle
    :param m: 2d map where 0 is an empty cell and one is an obstacle
    :param start: start Point
    :param finish: finish Point
    :return: True if path collides with obstacle and False otherwise
    """
    x_list, y_list = get_line_points(start, finish)
    for x, y in zip(x_list, y_list):
        if m[y, x] == 1:
            return True
    return False


def calculate_angle(p1: Point, p2: Point, p3: Point) -> float:
    """
    The function calculates angle between two line segments
    :param p1: Point. line1 start
    :param p2: Point. line1 finish
    :param p3: Point. line2 start
    :param p4: Point. line2 finish
    :return: float. angle between segments
    """
    cos = (p2.x-p1.x) * (p3.x - p2.x) + (p2.y-p1.y) * (p3.y - p2.y)
    cos /= np.sqrt((p3.x - p2.x) * (p3.x - p2.x) + (p3.y - p2.y) * (p3.y - p2.y))
    cos /= np.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y))
    if cos > 1.:
        cos = 1.
    elif cos < -1.:
        cos = -1
    return np.arccos(cos) * 180 / np.pi
